# MongoDb Replica Set

This guide will walk you through the basics of mongodb replica set. To get started with this guide basic knowledge of mongodb and linux cli are required.

# Environment Setup

Install virtual box and vagrant. Then use the files provided in the setup folder to bring up the vagrant environment. You can do this by copying the files to a directory where you want to setup your environment the run the following commands:
```
vagrant up --provision
```

This will setup the virtual environment. Note that it might take a few minutes. Next ssh into this environment:
```
vagrant ssh
```

# Initiating a Replica Set

In order for the nodes to communicate using a key a keyfile must be created. Lets create this file inside the directory ```var/mongodb/pki``` and change its permissions so only the owner of the file can read it or write to it.

Create the directory
```
sudo mkdir -p /var/mongodb/pki
```

Change the owner of the directory
```
sudo chown vagrant:vagrant -R /var/mongodb
```

Create a keyfile
```
openssl rand -base64 741 > /var/mongodb/pki/keyfile
```

Give permissions (600 here means read permission)
```
chmod 600 /var/mongodb/pki/keyfile
```

Now that the keyfile is created lets setup a 3 node replica set. The configuration files of each node is provided in the config folder. Note that the mongod does not automatically create the dbPath or logPath directories so we have to create them first and change permissions.
```
sudo mkdir /var/mongodb/db
sudo chown vagrant:vagrant -R /var/mongodb/db
```

Start a mongod process using the ```mongod-repl-1.yaml``` file inside the config folder. It will start a mongod process on port 27001. This mongod process will act as the primary node in your replica set.
```
mongod -f mongod-repl-1.yaml
```

Now use the mongo shell to connect to this node and initiate your replica set with:
```
rs.initiate()
```

The node automatically configures a default replication configuration and elects itself as a primary. Use the followning command to check the status of the replica set:
```
rs.status()
```

While still connected to the primary node, create an admin user for your cluster using the localhost exception in the admin database.
```
use admin
db.createUser({ 
  user: 'repl-admin', 
  pwd: 'repl-pass', 
  roles: [{ role: 'root', db: 'admin' }] 
})
```

Now exit the mongo shell and start the other two mongod processes with their respective configuration files.
```
mongod -f mongod-repl-2.yaml
mongod -f mongod-repl-3.yaml
```

Next reconnect to your primary node by authenticating with the user created above and add the other two nodes to your replica set.
```
mongo --host 'mongodb-repl-set/192.168.103.100:27001' -u repl-admin -p repl-pass --authenticationDatabase admin
rs.add('192.168.103.100:27002')
rs.add('192.168.103.100:27003')
```

Once the other two members have been successfully added, run ```rs.status()``` to check that the members array has three nodes - one labeled PRIMARY and two labeled SECONDARY.